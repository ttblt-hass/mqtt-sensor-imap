"""MQTT Sensor Facebook module."""
import logging
import re
from typing import Dict

from fb_feed_gen import fetch
from bs4 import BeautifulSoup
from mqtt_hass_base.device import MqttDevice

from mqtt_sensor_imap.__version__ import VERSION
from mqtt_sensor_imap.common import render_value, clean_string


class FacebookFeedDevice(MqttDevice):
    """MQTT Sensor Facebook class."""

    def __init__(self, name: str, logger: logging.Logger, config: Dict):
        """Constructor."""
        MqttDevice.__init__(self, name, logger)
        self._config = config
        # Get that
        # self.mac = raw_device_info.mac_address
        self.sw_version = VERSION
        self.manufacturer = "ttblt-hass"
        self.model = "mqtt-sensor-feed-facebook"
        self.identifiers = self.name

    def add_entities(self):
        """Add Home Assistant entities."""
        for fb_conf in self._config.get("sensors", []):
            # TODO improve this cleanup
            cleaned_name = fb_conf["name"].lower().replace(" ", "_")
            entity_settings = {
                "device_class": fb_conf.get("device_class", None),
                "expire_after": fb_conf.get("expire_after", 0),
                "force_update": fb_conf.get("force_update", False),
            }
            if "icon" in fb_conf:
                entity_settings["icon"] = fb_conf["icon"]

            setattr(
                self,
                cleaned_name,
                self.add_entity(
                    "sensor",
                    " ".join(("facebook", cleaned_name)),
                    entity_settings,
                ),
            )

    def update(self):
        """Update Home Assistant entities."""
        for fb_conf in self._config.get("sensors", []):
            cleaned_name = fb_conf["name"].lower().replace(" ", "_")

            site_url = fetch.build_site_url(fb_conf["page"])
            data = fetch.get_remote_data(site_url)
            posts = fetch.extract_items(fb_conf["page"], data)
            # TODO parse all posts not read yet
            last_post = posts[0]
            # TODO find a way to read all the messages
            # use an other lib ?
            msg = BeautifulSoup(last_post["article"], "lxml").text
            # Filter messages and titles
            matched = False
            for filter_ in fb_conf["filters"]:
                if re.match(filter_, last_post["title"]) or re.match(filter_, msg):
                    matched = True
            if matched is False:
                continue

            template_data = {
                "title": last_post["title"],
                "date": last_post["date"].strftime("%d-%M-%Y %H:%M"),
                "message": msg,
            }
            # Build attributes
            attributes = {}
            for key, value in fb_conf.get("attributes", {}).items():
                attributes[key] = render_value(value, template_data)
            # Build state
            if "state" in fb_conf:
                state = render_value(fb_conf["state"], template_data)
            else:
                state = " - ".join((template_data["date"], template_data["title"]))
            state = clean_string(state)
            # TODO clean message
            getattr(self, cleaned_name).send_state(state, attributes)
            getattr(self, cleaned_name).send_available()
