"""Mqtt Kab switch module."""
import asyncio
import os

import yaml

import mqtt_hass_base
from mqtt_sensor_imap.facebook_device import FacebookFeedDevice
from mqtt_sensor_imap.imap_device import ImapFeedDevice
from mqtt_sensor_imap.const import MAIN_LOOP_WAIT_TIME


class MqttSensorFeed(mqtt_hass_base.MqttClientDaemon):
    """MQTT Sensor Feed."""

    def __init__(self):
        """Constructor."""
        mqtt_hass_base.MqttClientDaemon.__init__(self, "mqtt-sensor-feed")

    def read_config(self):
        """Read env vars."""
        self.config_file = os.environ["CONFIG_YAML"]
        with open(self.config_file) as fhc:
            self.config = yaml.safe_load(fhc)
        self.sync_frequency = int(
            self.config.get("sync_frequency", MAIN_LOOP_WAIT_TIME)
        )

        if "facebook" in self.config:
            self._fb_device = FacebookFeedDevice(
                self.name + "-facebook", self.logger, self.config["facebook"]
            )
            self._fb_device.add_entities()

        if "imap" in self.config:
            self._imap_device = ImapFeedDevice(
                self.name + "-imap", self.logger, self.config["imap"]
            )
            self._imap_device.add_entities()

    async def _init_main_loop(self):
        """Init before starting main loop."""
        if "facebook" in self.config:
            self._fb_device.set_mqtt_client(self.mqtt_client)
        if "imap" in self.config:
            self._imap_device.set_mqtt_client(self.mqtt_client)
            await self._imap_device.imap_connect()

    async def _main_loop(self):
        """Run main loop."""
        if "facebook" in self.config:
            self._fb_device.update()
        if "imap" in self.config:
            await self._imap_device.update()

        i = 0
        while i < MAIN_LOOP_WAIT_TIME and self.must_run:
            await asyncio.sleep(1)
            i += 1

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""
        self._fb_device.register()
        self._imap_device.register()

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""
        self._fb_device.subscribe()
        self._imap_device.subscribe()

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""
        self._fb_device.unregister()
        self._imap_device.unregister()

    def _on_message(self, client, userdata, msg):
        """Do nothing."""

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        self._imap_device.stop()
