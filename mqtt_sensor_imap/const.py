"""Mqtt feed const module."""

MAIN_LOOP_WAIT_TIME = 60

DEVICE_CLASSES = (
    None,  # Generic sensor. This is the default and doesn’t need to be set.
    "battery",  # Percentage of battery that is left.
    "humidity",  # Percentage of humidity in the air.
    "illuminance",  # The current light level in lx or lm.
    "signal_strength",  # Signal strength in dB or dBm.
    "temperature",  # Temperature in °C or °F.
    "power",  # Power in W or kW.
    "pressure",  # Pressure in hPa or mbar.
    "timestamp",  # Datetime object or timestamp string.
)

# TODO use a lib for that
CLEAN_MAP = (
    ("?= =?utf-8?Q?", ""),
    ("=?utf-8?Q?", ""),
    ("=\r\n", ""),
    ("=E2=80=99", "'"),
    ("=E2=80=93_", "- "),
    ("=E2=80=93", "-"),
    ("=E2=80=B2", "'"),
    ("=C3=BB", "u"),
    ("=C2=BB", '"'),
    ("=C2=AB", '"'),
    ("=C3=A9", "e"),
    ("=C3=A8", "e"),
    ("=C3=A0", "a"),
    ("=C3=AA", "e"),
    ("=C2=A0", " "),
    ("=27", "'"),
    ("=09", ""),
    ("?= ", " "),
    ("?=", ""),
    ("‼️", "!"),
    ("❄️", ""),
    ("🚜", ""),
)
