"""Common functions."""
from jinja2 import Template

from mqtt_sensor_imap.const import CLEAN_MAP


def clean_string(string):
    """Try to clean email string.

    .. todo:: use a lib for that
    """
    if isinstance(string, bytes):
        string = string.decode()
    for old_str, new_str in CLEAN_MAP:
        string = string.replace(old_str, new_str)
    return string


def render_value(template_str, data):
    """Render the value of the sensor."""
    template = Template(template_str)
    return template.render(**data)
