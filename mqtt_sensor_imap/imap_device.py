"""Mqtt IMAP module."""
import datetime
import email
import imaplib
import os
import traceback
import shelve
import logging
from typing import Dict

from imapclient import IMAPClient
from mqtt_hass_base.device import MqttDevice

from mqtt_sensor_imap.__version__ import VERSION
from mqtt_sensor_imap.common import render_value, clean_string


class ImapFeedDevice(MqttDevice):
    """MQTT Sensor IMAP."""

    def __init__(self, name: str, logger: logging.Logger, config: Dict):
        """Constructor."""
        MqttDevice.__init__(self, name, logger)
        self._config = config
        # Get that
        # self.mac = raw_device_info.mac_address
        self.sw_version = VERSION
        self.manufacturer = "ttblt-hass"
        self.model = "mqtt-sensor-feed-imap"
        self.identifiers = self.name
        self.imap_client = None

        # Get username from env vars
        if "MAIL_USERNAME" in os.environ:
            self._config["username"] = os.environ["MAIL_USERNAME"]
        # Get username from env vars
        if "MAIL_PASSWORD" in os.environ:
            self._config["password"] = os.environ["MAIL_PASSWORD"]

        self.message_dates_cache = self._config.get(
            "cache_file", "/tmp/mqtt_sensor_imap.db"
        )
        if "CACHE_FILE" in os.environ:
            self.message_dates_cache = os.environ["CACHE_FILE"]

    def add_entities(self):
        """Add Home Assistant entities."""
        for imap_conf in self._config.get("sensors", []):
            cleaned_name = imap_conf["name"].lower().replace(" ", "_")
            entity_settings = {
                "device_class": imap_conf.get("device_class", None),
                "expire_after": imap_conf.get("expire_after", 0),
                "force_update": imap_conf.get("force_update", False),
            }
            if "icon" in imap_conf:
                entity_settings["icon"] = imap_conf["icon"]

            setattr(
                self,
                cleaned_name,
                self.add_entity(
                    "sensor",
                    " ".join(("imap", cleaned_name)),
                    entity_settings,
                ),
            )

    async def update(self):
        """Update Home Assistant entities."""
        for imap_conf in self._config["sensors"]:
            cleaned_name = imap_conf["name"].lower().replace(" ", "_")
            try:
                message_ids = []
                self.imap_client.select_folder(imap_conf["folder"])
                last_msg_date = self._get_last_message_date(imap_conf["name"])
                if "filters" not in imap_conf:
                    mail_filter = ["SINCE", last_msg_date]
                    self.logger.debug(
                        "Search for emails in folder %s with filter %s",
                        imap_conf["folder"],
                        mail_filter,
                    )
                    message_ids.extend(self.imap_client.search(mail_filter))
                else:
                    for mail_filter in imap_conf["filters"]:
                        mail_filter.append("SINCE")
                        mail_filter.append(last_msg_date)
                        self.logger.debug(
                            "Search for emails in folder %s with filter %s",
                            imap_conf["folder"],
                            mail_filter,
                        )
                        message_ids.extend(self.imap_client.search(mail_filter))
                messages = self.imap_client.fetch(message_ids, ["ENVELOPE", "RFC822"])
                # Order messages by date and filter messages by datetime
                msg_list = sorted(
                    [
                        m
                        for m in messages.values()
                        if m[b"ENVELOPE"].date > last_msg_date
                    ],
                    key=lambda x: x[b"ENVELOPE"].date,
                    reverse=False,
                )
                if msg_list:
                    self.logger.info(
                        "%s has %d new message(s)", imap_conf["name"], len(msg_list)
                    )
                for data in msg_list:
                    template_data = {}
                    template_data["subject"] = clean_string(data[b"ENVELOPE"].subject)
                    template_data["date"] = data[b"ENVELOPE"].date
                    parsed_email = email.message_from_bytes(data[b"RFC822"])
                    for part in parsed_email.walk():
                        if part.get_content_type() == "text/plain":
                            template_data["body"] = clean_string(part.get_payload())
                            break
                    if "body" not in template_data:
                        self.logger.error("No body found in the email")
                        continue

                    # Build attributes
                    attributes = {}
                    for key, value in imap_conf.get("attributes", {}).items():
                        attributes[key] = render_value(value, template_data)

                    state = render_value(imap_conf["state"], template_data)
                    getattr(self, cleaned_name).send_state(state, attributes)
                    getattr(self, cleaned_name).send_available()
                    self._save_last_message_date(
                        imap_conf["name"], template_data["date"]
                    )

                    # self.mqtt_client.publish(topic="{}/state".format(topic_m_base),
                    #                         payload="{}".format(value))
            except imaplib.IMAP4.abort as exp:
                self.logger.warning("IMAP error: %s", exp)
                traceback.print_exc()
                self.logger.warning("IMAP Reconnection")
                self.imap_client.logout()
                await self.imap_connect()
            except Exception as exp:  # pylint: disable=W0703
                self.logger.warning("Uknown error: %s", exp)
                traceback.print_exc()
                self.logger.warning("IMAP Reconnection")
                self.imap_client.logout()
                await self.imap_connect()

    def stop(self):
        """Run after the end of the main loop."""
        self.imap_client.logout()

    def _get_last_message_date(self, sensor_name):
        """Get last email ID."""
        with shelve.open(self.message_dates_cache) as ids_by_name:
            message_id = ids_by_name.get(
                sensor_name, datetime.datetime.now() - datetime.timedelta(days=2)
            )
        return message_id

    def _save_last_message_date(self, sensor_name, message_date):
        """Save last email ID."""
        with shelve.open(self.message_dates_cache) as ids_by_name:
            ids_by_name[sensor_name] = message_date

    async def imap_connect(self):
        """Init before starting main loop."""
        self.imap_client = IMAPClient(
            host=self._config["host"], port=self._config.get("port", 993), ssl=True
        )
        self.imap_client.login(self._config["username"], self._config["password"])
        for sensor in self._config["sensors"]:
            folder_search = self.imap_client.list_folders(pattern=sensor["folder"])
            if not folder_search:
                raise Exception("Folder {} not found".format(sensor["folder"]))
