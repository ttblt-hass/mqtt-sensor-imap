"""Module defining entrypoint."""
import asyncio

from mqtt_sensor_imap import daemon


def main():
    """Entrypoint function."""
    dev = daemon.MqttSensorFeed()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
