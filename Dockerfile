FROM python:3.8-slim-stretch
  
COPY requirements.txt /requirements.txt

RUN apt-get update -y && \
    apt-get upgrade -y && \
    pip install pip --upgrade && \
    pip install -r /requirements.txt && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt

RUN mkdir -p /app
WORKDIR /app

COPY Dockerfile /Dockerfile
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY requirements.txt  setup.py  test_requirements.txt /app/
RUN pip install -r /app/requirements.txt
COPY mqtt_sensor_imap /app/mqtt_sensor_imap

RUN python setup.py install

ENV MQTT_USERNAME=username
ENV MQTT_PASSWORD=password
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV ROOT_TOPIC=homeassistant
ENV MQTT_NAME=mqtt-sensor-imap
ENV LOG_LEVEL=INFO
ENV CACHE_FILE /tmp/mqtt_sensor_imap.db

CMD sh /entrypoint.sh
